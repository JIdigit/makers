from django.shortcuts import render
from django.views.generic import TemplateView


class EmirTemplateView(TemplateView):
    template_name = 'emir.html'

